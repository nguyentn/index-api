﻿using Google.Apis.Auth.OAuth2;
using IndexingWeb2.Models;
using Newtonsoft.Json;
using Quartz;
using System.IO;
using System.Net;
using System.Text;
using System;
using System.Threading.Tasks;
using Quartz.Logging;
using Microsoft.Extensions.Configuration;

namespace IndexingWeb2.BackgroundTasks
{
    public class AutoRefreshTokenJob : IJob
    {
        private readonly IConfiguration _configuration;
        public AutoRefreshTokenJob(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task Execute(IJobExecutionContext context)
        {

            if (_configuration.GetValue<string>("BackgroundTask:Enable") == "false")
            {
                return;
            }

            string clientId = "1051669495153-4sl65qfe2cskli3180h0eagbsm9d0ap2.apps.googleusercontent.com";
            string clientSecret = "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";
            string refreshToken = "1//0ekFkEntCAv9aCgYIARAAGA4SNwF-L9Irxa6IEDCDGHSl9zpx5stiacSA2Vf_DkkeGeZzcp55O4rEhofG4NaT7X5zuzPLQClgssc";
            var token = JsonConvert.DeserializeObject<TokenModel>(System.IO.File.ReadAllText(@"Token/token.json"));
            string refreshTokenUri = "https://www.googleapis.com/oauth2/v4/token";
            string refreshTokenRequestBody = string.Format("client_secret={0}&grant_type=refresh_token&refresh_token={1}&client_id={2}",
                    clientSecret,
                    refreshToken,
                    clientId);

            HttpWebRequest refreshTokenRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            refreshTokenRequest.Method = "POST";
            refreshTokenRequest.ContentType = "application/x-www-form-urlencoded";
            refreshTokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] refreshTokenRequestBodyBytes = Encoding.ASCII.GetBytes(refreshTokenRequestBody);
            refreshTokenRequest.ContentLength = refreshTokenRequestBodyBytes.Length;
            using (Stream requestStream = refreshTokenRequest.GetRequestStream())
            {
                await requestStream.WriteAsync(refreshTokenRequestBodyBytes, 0, refreshTokenRequestBodyBytes.Length);
            }
            try
            {
                // gets the response
                WebResponse tokenResponse = await refreshTokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    Console.WriteLine(responseText);
                    System.IO.File.WriteAllText(@"Token/token.json", responseText);
                    // converts to dictionary
                    // Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);
                    //string accessToken = tokenEndpointDecoded["access_token"];
                    Log(string.Format("Auto refresh token job finished at {0}", DateTime.UtcNow));
                    Log(responseText);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                            Log("Auto refresh token job failed");
                            Log(responseText);
                        }
                    }

                }
            }
        }

        private void Log(string output)
        {
            Console.WriteLine(output);
        }
    }
}
