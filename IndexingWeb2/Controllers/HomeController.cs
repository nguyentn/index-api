﻿using IndexingWeb2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IndexingWeb2.Controllers
{
    public static class Store
    {
        public static string codeVerifier;
    }
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;
        private string _host;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _host = configuration.GetValue<string>("HostUrl");
        }

        public IActionResult Index()
        {
            const string AuthorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";


            string clientId = "1051669495153-4sl65qfe2cskli3180h0eagbsm9d0ap2.apps.googleusercontent.com";
            string clientSecret = "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";


            // Generates state and PKCE values.
            string state = Utils.GenerateRandomDataBase64url(32);
            string codeVerifier = Utils.GenerateRandomDataBase64url(32);
            string codeChallenge = Utils.Base64UrlEncodeNoPadding(Utils.Sha256Ascii(codeVerifier));
            const string codeChallengeMethod = "S256";
            Store.codeVerifier = codeVerifier;

            // Creates a redirect URI using an available port on the loopback address.
            string redirectUri = _host + "/signin-google/";
            //Log("redirect URI: " + redirectUri);

            //// Creates an HttpListener to listen for requests on that redirect URI.
            //var http = new HttpListener();
            //http.Prefixes.Add(redirectUri);
            //Log("Listening..");
            //http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&scope=openid%20profile%20https://www.googleapis.com/auth/indexing&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                AuthorizationEndpoint,
                Uri.EscapeDataString(redirectUri),
                clientId,
                state,
                codeChallenge,
                codeChallengeMethod);

            ViewBag.Link = authorizationRequest;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
