﻿using Google.Apis.Auth.OAuth2.Requests;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using static Google.Apis.Requests.BatchRequest;
using Newtonsoft.Json.Linq;
using IndexingWeb2.Models;

namespace IndexingWeb2.Controllers
{
    [ApiController]
    [Route("refresh")]
    public class RefreshTokenController : Controller
    {
        public async Task<ActionResult> RefreshToken()
        {
            string clientId = "1051669495153-4sl65qfe2cskli3180h0eagbsm9d0ap2.apps.googleusercontent.com";
            string clientSecret = "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";

            await RefreshAccessTokenAsync(clientSecret, clientId);

            return View();
        }

        async Task RefreshAccessTokenAsync(string clientSecret, string clientId)
        {
            var refreshToken = System.IO.File.ReadAllText(@"Token/RefreshToken.txt");
            string refreshTokenUri = "https://www.googleapis.com/oauth2/v4/token";
            string refreshTokenRequestBody = string.Format("client_secret={0}&grant_type=refresh_token&refresh_token={1}&client_id={2}",
                    clientSecret,
                    refreshToken,
                    clientId);

            HttpWebRequest refreshTokenRequest = (HttpWebRequest)WebRequest.Create(refreshTokenUri);
            refreshTokenRequest.Method = "POST";
            refreshTokenRequest.ContentType = "application/x-www-form-urlencoded";
            refreshTokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] refreshTokenRequestBodyBytes = Encoding.ASCII.GetBytes(refreshTokenRequestBody);
            refreshTokenRequest.ContentLength = refreshTokenRequestBodyBytes.Length;
            using (Stream requestStream = refreshTokenRequest.GetRequestStream())
            {
                await requestStream.WriteAsync(refreshTokenRequestBodyBytes, 0, refreshTokenRequestBodyBytes.Length);
            }
            try
            {
                // gets the response
                WebResponse tokenResponse = await refreshTokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    Console.WriteLine(responseText);
                    System.IO.File.WriteAllText(@"Token/token.json", responseText);
                    // converts to dictionary
                    // Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);
                    //string accessToken = tokenEndpointDecoded["access_token"];
                    ViewBag.Response = "Token has been refreshed successfully.";
                    ViewBag.StatusCode = System.Net.HttpStatusCode.OK;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                            ViewBag.Response = responseText;
                            ViewBag.StatusCode = response.StatusCode;
                        }
                    }

                }
            }

        }

    }


}
