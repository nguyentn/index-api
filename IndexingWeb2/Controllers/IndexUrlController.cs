﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System;
using IndexingWeb2.Data;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using IndexingWeb2.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Policy;

namespace IndexingWeb2.Controllers
{
    [Route("index-api")]
    public class IndexUrlController : Controller
    {
        private readonly IdDbContext _dbContext;
        public IndexUrlController(IdDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ActionResult> IndexUrl(string txtSearch, int page)
        {
            await GetPage(txtSearch, page);
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> IndexUrl(string url, string requestType)
        {
            await SendIndexRequest(url, requestType);
            ViewBag.Url = url;
            await GetPage(null, 1);
            return View();
        }

        async Task GetPage(string txtSearch, int page)
        {
            if (page <= 0)
            {
                page = 1;
            }
            var size = 10;
            var data = await _dbContext.IndexResponse.ToListAsync();
            if (!string.IsNullOrEmpty(txtSearch))
            {
                ViewBag.txtSearch = txtSearch;
                data = data.OrderByDescending(x => x.NotifyTime).Where(x => x.Url.Contains(txtSearch)).ToList();
            }
            var totalPages = (int)Math.Ceiling(data.Count() / (double)size);
            var totalElements = data.Count();
            data = data.OrderByDescending(x => x.NotifyTime).Skip((page - 1) * size).Take(size).ToList();

            ViewBag.Size = size;
            ViewBag.Page = page;
            ViewBag.TotalElements = totalElements;
            ViewBag.TotalPages = totalPages;
            ViewBag.Data = data;
        }

        async Task<string> SendIndexRequest(string url, string requestType)
        {
            var type = "URL_UPDATED";
            if (requestType == "delete")
                type = "URL_DELETED";

            ViewBag.Type = type;
            var indexRequestUri = "https://indexing.googleapis.com/v3/urlNotifications:publish";
            
            var accessToken = JsonConvert.DeserializeObject<TokenModel>(System.IO.File.ReadAllText(@"Token/token.json")).AccessToken;
            // Get & add authorization header
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(indexRequestUri);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "Accept=text/html,application/xhtml+xml,application/xml,application/json;q=0.9,*/*;q=0.8";
            request.Headers.Add(string.Format("Authorization: Bearer {0}", accessToken));
            var requestBody = JsonConvert.SerializeObject(new IndexingRequestModel { url = url, type = type }).ToString();
            byte[] requestBodyBytes = Encoding.ASCII.GetBytes(requestBody);
            request.ContentLength = requestBodyBytes.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                await requestStream.WriteAsync(requestBodyBytes, 0, requestBodyBytes.Length);
            }

            try
            {
                // gets the response
                WebResponse tokenResponse = await request.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    Console.WriteLine(responseText);

                    var response = new IndexResponse
                    {
                        Url = url,
                        Type = type,
                        Response = responseText.ToString(),
                        NotifyTime = DateTime.UtcNow,
                    };
                    _dbContext.IndexResponse.Add(response);
                    await _dbContext.SaveChangesAsync();

                    ViewBag.Response = "Submitted successfully";

                    return responseText;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        //Log("HTTP: " + response.StatusCode);
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();

                            ViewBag.Response = responseText;
                        }
                    }

                }
                return ex.Message;
            }
        }
    }
}
