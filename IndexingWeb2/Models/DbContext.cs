﻿using Microsoft.Extensions.Logging.Debug;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using IndexingWeb2.Models;

namespace IndexingWeb2.Data
{
    public class IdDbContext : DbContext
    {
        private string connectionString;

        private static readonly ILoggerFactory loggerFactory = new LoggerFactory(new[] {
              new DebugLoggerProvider()
        });

        public IdDbContext()
        {
            connectionString = "Host=dev1.geneat.vn;Port=35432;Database=Index;Username=postgres;Password=1Qaz2wsx";
        }
        public virtual DbSet<IndexResponse> IndexResponse { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(connectionString);
            }
            optionsBuilder.UseLoggerFactory(loggerFactory).EnableSensitiveDataLogging();
        }
    }
}
