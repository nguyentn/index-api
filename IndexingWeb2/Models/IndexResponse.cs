﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IndexingWeb2.Models
{
    [Table("IndexResponse")]
    public class IndexResponse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
        public string Response { get; set; }
        public DateTime? NotifyTime { get; set; }
    }

    public class IndexingRequestModel
    {
        public string url { get; set; }
        public string type { get; set; }
    }
}
